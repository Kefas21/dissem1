/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Semka1.main;

import Semka1.monteCarlo.ExperimentResult;
import Semka1.monteCarlo.MonteCarlo;
import Semka1.monteCarlo.MonteListener;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *  MAIN/GUI class
 * @author peter
 */
public class MontyHallFx extends Application implements MonteListener<ExperimentResult> {

    XYChart.Series<Number, Number> seriesChanged;
    XYChart.Series<Number, Number> seriesNotChanged;
    LineChart<Number, Number> linechart;
    Thread simThread;
    Label montyChangedLabel;
    Label montyNotChangedLabel;

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Monty Hall");
        primaryStage.setWidth(1200);
        primaryStage.setHeight(600);

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(50);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Monty Hall");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label iterationLabel = new Label("Number of iterations:");
        grid.add(iterationLabel, 0, 1);

        TextField iterationField = new TextField();
        iterationField.setText("100000");
        iterationField.setMaxWidth(75);
        grid.add(iterationField, 1, 1);

        Label doorLabel = new Label("Number of doors:");
        grid.add(doorLabel, 0, 2);

        //Choice box for location 
        ChoiceBox doorChoiceBox = new ChoiceBox();
        doorChoiceBox.getItems().addAll("3", "4", "5", "6", "7", "8", "9", "10");
        doorChoiceBox.setValue("3");
        grid.add(doorChoiceBox, 1, 2);

        //Toggle group of radio buttons      
        Label speedLabel = new Label("Simulation speed");
        grid.add(speedLabel, 2, 0);
        ToggleGroup speedGroup = new ToggleGroup();
        RadioButton normalSpeedRadio = new RadioButton("Normal");
        normalSpeedRadio.setSelected(true);
        normalSpeedRadio.setToggleGroup(speedGroup);
        grid.add(normalSpeedRadio, 2, 1);
//        RadioButton slowSpeedRadio = new RadioButton("Slow");
//        slowSpeedRadio.setToggleGroup(speedGroup);
//        grid.add(slowSpeedRadio, 2, 2);
//        RadioButton fastSpeedRadio = new RadioButton("Fast");
//        fastSpeedRadio.setToggleGroup(speedGroup);
//        grid.add(fastSpeedRadio, 2, 3);

        Label actionLabel = new Label("Actions");
        grid.add(actionLabel, 3, 0);
        Button startBtn = new Button("Start");
//        Button pauseBtn = new Button("Pause");
//        Button resetBtn = new Button("Reset");
        startBtn.setMinWidth(100);
//        pauseBtn.setMinWidth(100);
//        resetBtn.setMinWidth(100);
        grid.add(startBtn, 3, 1);
//        grid.add(pauseBtn, 3, 2);
//        grid.add(resetBtn, 3, 3);

        Label chartViewLabel = new Label("Chart View");
        grid.add(chartViewLabel, 4, 0);
        ToggleGroup chartViewGroup = new ToggleGroup();
        RadioButton bothRadio = new RadioButton("Both Lines");
        bothRadio.setSelected(true);
        bothRadio.setToggleGroup(chartViewGroup);
        grid.add(bothRadio, 4, 1);
        RadioButton changedRadio = new RadioButton("Changed Door Line");
        changedRadio.setToggleGroup(chartViewGroup);
        grid.add(changedRadio, 4, 2);
        RadioButton notChangedRadio = new RadioButton("Not Changed Door Line");
        notChangedRadio.setToggleGroup(chartViewGroup);
        grid.add(notChangedRadio, 4, 3);

        //show probability value
        montyChangedLabel = new Label("Changed Door Probability:");
        montyChangedLabel.setFont(Font.font("Tahoma", FontWeight.NORMAL, 17));
        grid.add(montyChangedLabel, 5, 0, 4, 1);
        montyNotChangedLabel = new Label("Not Changed Door Probability:");
        montyNotChangedLabel.setFont(Font.font("Tahoma", FontWeight.NORMAL, 17));
        grid.add(montyNotChangedLabel, 5, 1, 4, 1);

        setUpLineChart(0);

        //start button event - simulation start
        startBtn.setOnAction(event -> {
            MonteCarlo mc = new MonteCarlo(Integer.parseInt(iterationField.getText()),
                    Integer.parseInt(doorChoiceBox.getValue().toString()), this);
            mc.simulation();
            linechart.setVisible(false);

            if (bothRadio.isSelected()) {
                setUpLineChart(0);
            }
            if (notChangedRadio.isSelected()) {
                setUpLineChart(2);
            }
            if (changedRadio.isSelected()) {
                setUpLineChart(1);
            }

            grid.add(linechart, 0, 4, 10, 1);
        });

        Scene scene = new Scene(grid);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Adding/updating results in chart
     * @param result
     */
    @Override
    public void sendResult(ExperimentResult result) {
        Platform.runLater(() -> {
            if (result.isChangeDoor()) {
                seriesChanged.getData().add(new XYChart.Data<Number, Number>(result.getIteration(), result.getProbability()));
                montyChangedLabel.setText("Changed Door Probability: " + result.getProbability());
                //System.out.println(result.getIteration()+": Zmena s pr: "+result.getProbability());
            } else {
                seriesNotChanged.getData().add(new XYChart.Data<Number, Number>(result.getIteration(), result.getProbability()));
                montyNotChangedLabel.setText("Not Changed Door Probability: " + result.getProbability());
                //System.out.println(result.getIteration()+": Ziadna zmena s pr: "+result.getProbability());
            }
        });
    }

    private void setUpLineChart(int line) {
        //set up chart
        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Replications");
        xAxis.setAutoRanging(true);
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Probability");
        yAxis.setAutoRanging(true);
        yAxis.forceZeroInRangeProperty().set(false);

        linechart = new LineChart<>(xAxis, yAxis);
        linechart.setCreateSymbols(false);
        this.seriesChanged = new XYChart.Series<>();
        this.seriesNotChanged = new XYChart.Series<>();
        seriesChanged.setName("Monty Hall Changed Probability");
        seriesNotChanged.setName("Monty Hall Not Changed Probability");
        if (line == 1) {
            linechart.getData().add(seriesChanged);
        }
        if (line == 2) {
            linechart.getData().add(seriesNotChanged);
        }
        if (line == 0) {
            linechart.getData().add(seriesChanged);
            linechart.getData().add(seriesNotChanged);
        }
    }

}
