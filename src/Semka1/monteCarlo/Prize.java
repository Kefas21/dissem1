/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Semka1.monteCarlo;

/**
 *  Enum Class representing prize behind the doors in Monty Hall problem
 * @author peter
 */
public enum Prize {

    /**
     *
     */
    CAR,

    /**
     *
     */
    ANIMAL
}
