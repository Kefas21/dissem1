/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Semka1.monteCarlo;

/**
 *  Class represents result from Monte Carlo simulation
 * @author peter
 */
public class ExperimentResult {
    private int iteration;
    private double probability;
    private boolean changeDoor;

    /**
     *
     * @param iteration
     * @param win
     * @param changeDoor
     */
    public ExperimentResult(int iteration, int win, boolean changeDoor) {
        this.iteration = iteration;
        this.probability =(double) win/iteration;
        this.changeDoor = changeDoor;
    }

    /**
     *
     * @return
     */
    public int getIteration() {
        return iteration;
    }

    /**
     *
     * @param iteration
     */
    public void setIteration(int iteration) {
        this.iteration = iteration;
    }

    /**
     *
     * @return
     */
    public double getProbability() {
        return probability;
    }

    /**
     *
     * @param probability
     */
    public void setProbability(double probability) {
        this.probability = probability;
    }

    /**
     *
     * @return
     */
    public boolean isChangeDoor() {
        return changeDoor;
    }

    /**
     *
     * @param changeDoor
     */
    public void setChangeDoor(boolean changeDoor) {
        this.changeDoor = changeDoor;
    }
    
}
