/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Semka1.monteCarlo;

/**
 *  Class represents doors in Monty Hall Problem
 * @author peter
 */
public class Door {
    private Prize prize;
    
    /**
     *
     * @param p
     */
    public Door(Prize p){
        this.prize=p;
    }

    /**
     *
     * @return
     */
    public Prize getPrize() {
        return prize;
    }

    /**
     *
     * @param prize
     */
    public void setPrize(Prize prize) {
        this.prize = prize;
    }
    
}
