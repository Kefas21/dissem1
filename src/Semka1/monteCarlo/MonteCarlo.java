/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Semka1.monteCarlo;

import Semka1.core.Core;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 *  Class represents Monte Carlo simulation
 * @author peter
 */
public class MonteCarlo extends Core {

    private Random randomSeed;
    private Random randomChangedChosenDoor;
    private Random randomOpen;
    private Random randomChange;
    private int winsNotChanged;
    private int winsChanged;
    private int doorNumber;
    private int chosenDoor;
    private ArrayList doors;
    private MonteListener<ExperimentResult> listener;
    ExperimentResult resultChanged;
    ExperimentResult resultNotChanged;

    /**
     *
     * @param reps
     * @param door
     * @param list
     */
    public MonteCarlo(int reps, int door, MonteListener list) {
        super(reps);
        this.doorNumber = door;
        this.listener = list;
    }

    /**
     *
     */
    public MonteCarlo() {
        super();
        this.doorNumber = 3;
    }

    /**
     * Initialize attributes - mostly random number generators
     */
    @Override
    public void beforeSimulation() {
        randomSeed = new Random(System.currentTimeMillis());
        randomChangedChosenDoor = new Random(randomSeed.nextLong());
        randomOpen = new Random(randomSeed.nextLong());
        randomChange = new Random(randomSeed.nextLong());
        winsChanged = 0;
        winsNotChanged = 0;
        doors = new ArrayList();
    }

    /**
     * Generating doors for Monty Hall problem
     */
    @Override
    public void beforeReplication() {
        doors.clear();
        doors.add(new Door(Prize.CAR));
        for (int j = 1; j < doorNumber; j++) {
            Door animal = new Door(Prize.ANIMAL);
            doors.add(animal);
        }
        Collections.shuffle(doors);
    }

    /**
     * One game in Monty Hall problem
     */
    @Override
    public void replication() {
        Door chosen;
        ArrayList doorToOpen = new ArrayList();
        ArrayList doorToChoose = new ArrayList();
        chosenDoor = randomChangedChosenDoor.nextInt(doors.size());
        for (int k = 0; k < doors.size(); k++) {
            Door d = (Door) doors.get(k);
            if (d.getPrize() != Prize.CAR && k != chosenDoor) {
                doorToOpen.add(d);
            }
        }
        int openDoor;
        openDoor = randomOpen.nextInt(doorToOpen.size());
        Door openedDoor = (Door) doorToOpen.get(openDoor);

        chosen = (Door) doors.get(chosenDoor);
        if (chosen.getPrize() == Prize.CAR) {
            winsNotChanged++;
        }

        for (int k = 0; k < doors.size(); k++) {
            Door d = (Door) doors.get(k);
            if (d != openedDoor && k != chosenDoor) {
                doorToChoose.add(d);
            }
        }
        chosenDoor = randomChange.nextInt(doorToChoose.size());

        chosen = (Door) doorToChoose.get(chosenDoor);
        if (chosen.getPrize() == Prize.CAR) {
            winsChanged++;
        }
    }
    
    /**
     * Sending results to chart by listener
     */
    @Override
    public void afterReplication(){
        if ((super.getActualReplication()+1)%1000==0 || super.getActualReplication()==0
                && (super.getActualReplication()>10000)
                ){
        resultChanged = new ExperimentResult(super.getActualReplication()+1, winsChanged, true);
        resultNotChanged = new ExperimentResult(super.getActualReplication()+1, winsNotChanged, false);
//        System.out.println(super.getI()+1+": Sanca, ked nezmeni dvere: " + resultNotChanged.getProbability());
//        System.out.println(super.getI()+1+": Sanca, ked zmeni dvere: " + resultChanged.getProbability());
        listener.sendResult(resultChanged);
        listener.sendResult(resultNotChanged);
        }
    }

    /**
     * Println results
     */
    @Override
    public void afterSimulation() {
        double notChangedWins = (double) winsNotChanged / super.getNumberOfReplications();
        double changedWins = (double) winsChanged / super.getNumberOfReplications();
        System.out.println("TOTAL Sanca, ked nezmeni dvere: " + notChangedWins);
        System.out.println("TOTAL Sanca, ked zmeni dvere: " + changedWins);
    }

}
