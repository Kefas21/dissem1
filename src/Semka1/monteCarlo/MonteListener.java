/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Semka1.monteCarlo;

/**
 *  Listener interface for results from Monte Carlo simulation
 * @author peter
 * @param <T>
 */
public interface MonteListener<T> {

    /**
     *
     * @param result
     */
    void sendResult(T result);
    
}
