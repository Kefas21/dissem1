/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Semka1.core;

/**
 *  Monte Carlo simulation core
 * @author peter
 */
public class Core {
    private int numberOfReplications;
    private int actualReplication;
    
    /**
     *
     * @param reps
     */
    public Core(int reps){
        this.numberOfReplications=reps;
        this.actualReplication=0;
    }
    
    /**
     *
     */
    public Core(){
        this.numberOfReplications=1000;
        this.actualReplication=0;
    }
    
    /**
     *
     */
    public void simulation(){
        beforeSimulation();
        
        for (actualReplication=0;actualReplication<numberOfReplications;actualReplication++){
            beforeReplication();
            replication();
            afterReplication();
        }       
        afterSimulation();
    }

    /**
     *
     */
    public void beforeSimulation() {
        
    }

    /**
     *
     */
    public void afterSimulation() {
        
    }

    /**
     *
     */
    public void beforeReplication() {
        
    }

    /**
     *
     */
    public void afterReplication() {
        
    }

    /**
     *
     */
    public void replication() {
        
    }

    /**
     *
     * @return
     */
    public int getActualReplication() {
        return actualReplication;
    }

    /**
     *
     * @return
     */
    public int getNumberOfReplications() {
        return numberOfReplications;
    }

    /**
     *
     * @param numberOfReplications
     */
    public void setNumberOfReplications(int numberOfReplications) {
        this.numberOfReplications = numberOfReplications;
    }
    
}
